//
//  UIViewController+Parent.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 08/09/21.
//

import UIKit

extension UIViewController {
    
    func remove() {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
}
