//
//  HomeFactory.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 03/09/21.
//

import UIKit

class HomeFactory {
    static func home(delegate: HomeViewControllerDelegateProtocol?) -> UIViewController {
        let viewModel = HomeViewModel()
        return HomeViewController(viewModel: viewModel, delegate: delegate)
    }
}
