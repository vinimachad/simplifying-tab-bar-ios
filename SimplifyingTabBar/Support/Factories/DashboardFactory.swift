//
//  DashboardFactory.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 08/09/21.
//

import UIKit

class DashboardFactory {
    static func dashboard() -> UIViewController {
        DashboardViewController()
    }
}
