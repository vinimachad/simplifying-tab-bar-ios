//
//  ProfileFactory.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 07/09/21.
//

import UIKit

class ProfileFactory {
    static func profile() -> UIViewController {
        return ProfileViewController()
    }
}
