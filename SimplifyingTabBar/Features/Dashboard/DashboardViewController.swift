//
//  DashboardViewController.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 08/09/21.
//

import UIKit

protocol DashboardViewControllerDelegateProtocol: AnyObject {}

class DashboardViewController: UIViewController {
    
    // MARK: - Private properties
    
    private var delegate: DashboardViewControllerDelegateProtocol?
    
    // MARK: - Init
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
