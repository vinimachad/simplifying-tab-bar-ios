//
//  HomeViewModel.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 03/09/21.
//

import UIKit

protocol HomeProtocol: HomeViewModelProtocol {
    var onSelectHomeTab: ((HomeViewModel.Items) -> Void)? { get set }
}

class HomeViewModel {
    
    enum Items: CaseIterable {
        case dashboard
        case profile
    }
    
    // MARK: - Public properties
    
    var onSelectHomeTab: ((Items) -> Void)?
    
    private(set) lazy var tabs: [UITabBarItem] = {
        return homeTabs.enumerated().map {
            createTabBy(index: $0.offset, homeTab: $0.element)
        }
    }()
    
    // MARK: - Private properties
    
    private let homeTabs: [Items]
    
    // MARK: - Init
    
    init() {
        homeTabs = Items.allCases
    }
    
    private func createTabBy(index: Int, homeTab: Items) -> UITabBarItem {
        UITabBarItem(title: homeTab.title, image: homeTab.icon, tag: index)
    }    
}

extension HomeViewModel: HomeProtocol {
    
    func didSelectTabWith(tag: Int) {
        if tag < homeTabs.count {
            onSelectHomeTab?(homeTabs[tag])
        }
    }
}

extension HomeViewModel.Items {
    
    var title: String {
        switch self {
            case .dashboard: return "Dashboard"
            case .profile: return "Perfil"
        }
    }
    
    var icon: UIImage {
        switch self {
            case .dashboard: return UIImage(named: "home") ?? UIImage()
            case .profile: return UIImage(named: "avatar") ?? UIImage()
        }
    }
}
