//
//  ViewController.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 03/09/21.
//

import UIKit

protocol HomeViewModelProtocol {
    var tabs: [UITabBarItem] { get }
    func didSelectTabWith(tag: Int)
}

class HomeView: UIView  {

    // MARK: - UI Components
    
    @IBOutlet weak var tabBar: UITabBar!
    
    // MARK: - Private properties
    
    private var viewModel: HomeViewModelProtocol?
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    // MARK: - Bind
    
    func bindIn(viewModel: HomeViewModelProtocol) {
        self.viewModel = viewModel
        
        tabBar.items = viewModel.tabs
    }
    
    // MARK: - View setup
    
    private func setup() {
        setupTabBar()
    }
    
    private func setupTabBar() {
        tabBar.delegate = self
    }
    
    func addViewAboveTabBar(_ view: UIView) {
        addSubview(view)
        sendSubviewToBack(view)
        
        let constraints = [
            self.topAnchor.constraint(equalTo: self.topAnchor),
            self.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}

extension HomeView: UITabBarDelegate {
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("tag->", item.tag)
        viewModel?.didSelectTabWith(tag: item.tag)
    }
}
