//
//  HomeViewController.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 03/09/21.
//

import UIKit

protocol HomeViewControllerDelegateProtocol: AnyObject {
    func getViewControllerBy(tab: HomeViewModel.Items) -> UIViewController
}

class HomeViewController<ViewModel: HomeProtocol>: UIViewController {
    
    // MARK: - Private properties
    
    private var contentView: HomeView
    private var viewModel: ViewModel
    private weak var delegate: HomeViewControllerDelegateProtocol?
    private(set) var childViewController: UIViewController?
    
    // MARK: - Init
    
    init(viewModel: ViewModel, delegate: HomeViewControllerDelegateProtocol?) {
        self.viewModel = viewModel
        self.delegate = delegate
        contentView = HomeView.loadNib()
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }
    
    // MARK: - Bind
    
    private func bind() {
        contentView.bindIn(viewModel: viewModel)
        
        viewModel.onSelectHomeTab = { [weak self] tab in
            if let viewController = self?.delegate?.getViewControllerBy(tab: tab) {
                self?.addViewController(viewController)
            }
        }
    }
    
    private func addViewController(_ controller: UIViewController) {
        if let oldViewController = childViewController {
            oldViewController.remove()
        }
        
        addChild(controller)
        contentView.addViewAboveTabBar(controller.view)
        controller.didMove(toParent: self)
        childViewController = controller
    }
}
