//
//  ProfileViewController.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 07/09/21.
//

import UIKit

protocol ProfileViewControllerDelegateProtocol: AnyObject {}

class ProfileViewController: UIViewController {

    // MARK: - Private properties
    
    private var delegate: ProfileViewControllerDelegateProtocol?
    
    // MARK: - Init
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
