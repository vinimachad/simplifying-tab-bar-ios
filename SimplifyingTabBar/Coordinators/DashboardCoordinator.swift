//
//  DashboardCoordinator.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 08/09/21.
//

import UIKit

class DashboardCoordinator: CoordinatorProtocol {
    
    weak var childDelegate: ChildCoordinatorDelegate?
    var childCoordinator: CoordinatorProtocol?
    var containerViewController: UIViewController {
        dashboardController
    }
    
    private var dashboardController = DashboardFactory.dashboard()
}
