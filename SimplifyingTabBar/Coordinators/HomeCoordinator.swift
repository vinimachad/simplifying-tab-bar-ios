//
//  HomeCoordinator.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 03/09/21.
//

import UIKit

class HomeCoordinator: CoordinatorProtocol {
    
    // MARK: - Public properties
    
    weak var childDelegate: ChildCoordinatorDelegate?
    var childCoordinator: CoordinatorProtocol?
    var containerViewController: UIViewController {
        homeController
    }
    
    // MARK: - Private properties
    
    private lazy var homeController = HomeFactory.home(delegate: self)
    
    private lazy var profileCoordinator: ProfileCoordinator = {
        let coordinator = ProfileCoordinator()
        return coordinator
    }()
    
    private lazy var dashboardCoordinator: DashboardCoordinator = {
        let coordinator = DashboardCoordinator()
        return coordinator
    }()
}

// MARK: - HomeViewControllerDelegateProtocol

extension HomeCoordinator: HomeViewControllerDelegateProtocol {
    func getViewControllerBy(tab: HomeViewModel.Items) -> UIViewController {
        switch tab {
        case .profile:
            childCoordinator = profileCoordinator
            return profileCoordinator.containerViewController
        case .dashboard:
            childCoordinator = dashboardCoordinator
            return dashboardCoordinator.containerViewController
        }
    }
}
