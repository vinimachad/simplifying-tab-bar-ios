//
//  ProfileCoordinator.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 07/09/21.
//

import UIKit

class ProfileCoordinator: CoordinatorProtocol {
    
    weak var childDelegate: ChildCoordinatorDelegate?
    var childCoordinator: CoordinatorProtocol?
    var containerViewController: UIViewController {
       profileController
    }
    
    private var profileController = ProfileFactory.profile()
}
