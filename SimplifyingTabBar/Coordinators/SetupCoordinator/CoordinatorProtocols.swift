//
//  CoordinatorProtocols.swift
//  SimplifyingTabBar
//
//  Created by Vinicius Galhardo Machado on 03/09/21.
//

import UIKit

// MARK: - CoordinatorProtocol

protocol CoordinatorProtocol: ChildCoordinatorDelegate {
    var childDelegate: ChildCoordinatorDelegate? { get set }
    var childCoordinator: CoordinatorProtocol? { get set }
    var containerViewController: UIViewController { get }
}

// MARK: - ChildCoordinatorDelegate

protocol ChildCoordinatorDelegate: AnyObject {
    func finishedFlow()
}

extension ChildCoordinatorDelegate where Self: CoordinatorProtocol {
    
    func finishedFlow() {
        childCoordinator = nil
    }
}

// MARK: - DismissibleCoordinator

protocol DismissibleCoordinator {
    func dismiss(animated: Bool)
}

extension DismissibleCoordinator where Self: CoordinatorProtocol {
    
    func dismiss(animated: Bool = true) {
        containerViewController.dismiss(animated: animated) { [weak self] in
            self?.childDelegate?.finishedFlow()
        }
    }
}
